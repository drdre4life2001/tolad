
<!DOCTYPE HTML>
<html>
<head>
    <title>Ready To wear By DHA </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Ready to wear, Fashion Design, African Designs Fabric, Tailor Made,
Design your wear, Ankara top, Skirt, Gown, Lagos, African" />
    <link href="{{asset('css/bootstrap.css')}}" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Custom Theme files -->
    <link href="{{url('css/style.css')}}" rel='stylesheet' type='text/css' />
    <!-- Custom Theme files -->
    <!--webfont-->
    <link href='http://fonts.googleapis.com/css?family=Lato:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <!-- start menu -<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->->
   
    <link href="{{url('css/megamenu.css')}}" rel="stylesheet" type="text/css" media="all" />
    
    <link rel="stylesheet" href="{{asset('css/etalage.css')}}">
   
    <!--initiate accordion-->
    
</head>
<body>
@include('layouts.includes.navbar')
<div class="single_top">
    <div class="container">
        <div class="single_grid">
            <div class="grid images_3_of_2">
                <ul id="etalage">
                    <li>
                        <a href="optionallink.html">
                            <img class="etalage_thumb_image" src="{{asset('images/s1.jpg')}}" class="img-responsive" />
                            <img class="etalage_source_image" src="{{asset('images/s1.jpg')}}" class="img-responsive" title="" />
                        </a>
                    </li>
                    <li>
                        <img class="etalage_thumb_image" src="{{asset('images/s2.jpg')}}" class="img-responsive" />
                        <img class="etalage_source_image" src="{{asset('images/s2.jpg')}}" class="img-responsive" title="" />
                    </li>
                    <li>
                        <img class="etalage_thumb_image" src="{{asset('images/s3.jpg')}}" class="img-responsive"  />
                        <img class="etalage_source_image" src="{{asset('images/s3.jpg')}}"class="img-responsive"  />
                    </li>
                    <li>
                        <img class="etalage_thumb_image" src="{{asset('images/s4.jpg')}}" class="img-responsive"  />
                        <img class="etalage_source_image" src="{{asset('images/s4.jpg')}}"class="img-responsive"  />
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="desc1 span_3_of_2">
                <ul class="back">
                    <li><i class="back_arrow"> </i>Back to <a href="index.html">Men's Clothing</a></li>
                </ul>
                <h1>{{$product->name}}</h1>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum</p>
                <div class="dropdown_top">
                <form action="{{route('cart.store')}}" method="POST">
                    <!-- <div class="dropdown_left">
                        <select class="dropdown" tabindex="10" data-settings='{"wrapperClass":"metro1"}'>
                            <option value="0">Select size</option>
                            <option value="1">M</option>
                            <option value="2">L</option>
                            <option value="3">XL </option>
                            <option value="4">Fs</option>
                            <option value="5">S </option>
                        </select>
                    </div> -->
                    <div class="form-group" style="margin-left:10px;">
                    <label for="quantity"> Quantity:</label>    
                    <input name="quantity" type="number" min="1" value=""></div>
                    <ul class="color_list">
                        <li><a href="#"> <span class="color1"> </span></a></li>
                        <li><a href="#"> <span class="color2"> </span></a></li>
                        <li><a href="#"> <span class="color3"> </span></a></li>
                        <li><a href="#"> <span class="color4"> </span></a></li>
                        <li><a href="#"> <span class="color5"> </span></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="simpleCart_shelfItem">
                    <div class="price_single">
                        <div class="head"><h2><span class="amount item_price">&#x20a6;{{$product->price}}</span></h2></div>
                        <div class="head_desc"><a href="#">12 reviews</a><img src="{{asset('images/review.png')}}" alt=""/></li></div>
                        <div class="clearfix"></div>
                    </div>
                    <!--<div class="single_but"><a href="" class="item_add btn_3" value=""></a></div>-->
                  <!-- <div class="size_2-right"><a href="#" class="item_add item_add1 btn_5" type="sbmit" value="" />Add to Cart </a></div>  -->
                     {{csrf_field()}}
                     <input name="id" type="hidden" value="{{$product->id}}">
                     <input name="name" type="hidden" value="{{$product->name}}">
                     <input name="price" type="hidden" value="{{$product->price}}">
                     <div class="size_2-right"><button class=" btn btn-large btn-danger" type="submit">Add to Cart </button></div>                     </div>
                    </form>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
      <div>
       
    <h3 class="m_2">Related Products</h3>
    <div class="container">
        <div class="box_3">
            @foreach($maylike as $item)
            <div class="col-md-3">
                <div class="content_box"><a href="single.html">
                        <img src="{{asset('images/pic6.jpg')}}" class="img-responsive" alt="">
                    </a>
                </div>
                <h4><a href="single.html">{{$item->name}}</a></h4>
                <p>&#x20a6;{{$item->price}}</p>
            </div>
            @endforeach
            
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="footer_top">
            <div class="col-md-4 box_3">
                <h3>Our Stores</h3>
                <address class="address">
                    <p>9870 St Vincent Place, <br>Glasgow, DC 45 Fr 45.</p>
                    <dl>
                        <dt></dt>
                        <dd>Freephone:<span> +1 800 254 2478</span></dd>
                        <dd>Telephone:<span> +1 800 547 5478</span></dd>
                        <dd>FAX: <span>+1 800 658 5784</span></dd>
                        <dd>E-mail:&nbsp; <a href="mailto@example.com">info(at)buyshop.com</a></dd>
                    </dl>
                </address>
                <ul class="footer_social">
                    <li><a href=""> <i class="fb"> </i> </a></li>
                    <li><a href=""><i class="tw    <script type="text/javascript" src="{{asset('js/jquery-1.11.1.min.js')}}"></script>
                     "> </i> </a></li>
                    <li><a href=""><i class="go    <script type="text/javascript" src="{{asset('js/jquery-1.11.1.min.js')}}"></script>
                     ogle"> </i> </a></li>
                    <li><a href=""><i class="in    <script type="text/javascript" src="{{asset('js/jquery-1.11.1.min.js')}}"></script>
                    stagram"> </i> </a></li>
                </ul>    <script type="text/javascript" src="{{asset('js/jquery-1.11.1.min.js')}}"></script>

            </div>    <script type="text/javascript" src="{{asset('js/jquery-1.11.1.min.js')}}"></script>

            <div class="col-md-4 box_3">    <script type="text/javascript" src="{{asset('js/jquery-1.11.1.min.js')}}"></script>

                <h3>Blog Posts</h3>    <script type="text/javascript" src="{{asset('js/jquery-1.11.1.min.js')}}"></script>

                <h4><a href="#">Sed ut perspici    <script type="text/javascript" src="{{asset('js/jquery-1.11.1.min.js')}}"></script>
atis unde omnis</a></h4>
                <p>The standard chunk of Lorem     <script type="text/javascript" src="{{asset('js/jquery-1.11.1.min.js')}}"></script>
Ipsum used since the 1500s is reproduced</p>
                <h4><a href="#">Sed ut perspiciatis unde omnis</a></h4>
                <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</p>
                <h4><a href="#">Sed ut perspiciatis unde omnis</a></h4>
                <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</p>
            </div>
            <div class="col-md-4 box_3">
                <h3>Support</h3>
                <ul class="list_1">
                ti><a href="#">Terms & Conditions</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Payment</a></li>
                    <li><a href="#">Refunds</a></li>
                    <li><a href="#">Track Order</a></li>
                    <li><a href="#">Services</a></li>
                </ul>
                <ul class="list_1">
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Press</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Contact Us</a></li>
                </ul>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="footer_bottom">
            <div class="copy">
                <p>Copyright © {{ date('Y') }} DHA Couture .</a> </p>
            </div>
        </div>
    </div>
</div>

    <script type="text/javascript" src="{{asset('js/jquery-1.11.1.min.js')}}"></script>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <script src="{{asset('js/simpleCart.min.js')}}"> </script>
    <script src="{{asset('js/jquery.easydropdown.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/megamenu.js')}}"></script>
    <script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
    <script src="{{asset('js/jquery.etalage.min.js')}}"></script>
    <script>
        jQuery(document).ready(function($){

            $('#etalage').etalage({
                thumb_image_width: 300,
                thumb_image_height: 400,
                source_image_width: 900,
                source_image_height: 1200,
                show_hint: true,
                click_callback: function(image_anchor, instance_id){
                    alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
                }
            });

        });
    </script>
    <script type="text/javascript">
        $(function() {

            var menu_ul = $('.menu_drop > li > ul'),
                menu_a  = $('.menu_drop > li > a');

            menu_ul.hide();

            menu_a.click(function(e) {
                e.preventDefault();
                if(!$(this).hasClass('active')) {
                    menu_a.removeClass('active');
                    menu_ul.filter(':visible').slideUp('normal');
                    $(this).addClass('active').next().stop(true,true).slideDown('normal');
                } else {
                    $(this).removeClass('active');
                    $(this).next().stop(true,true).slideUp('normal');
                }
            });

        });
    </script>
</body>
</html>