@extends('layouts.app')
@section('content')
<div class="bg-light py-3">
  <div class="container">
    <div class="row">
      <div class="col-md-12 mb-0"><a href="index.html">Home</a> <span class="mx-2 mb-0">/</span> <a href="cart.html">Cart</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Checkout</strong></div>
    </div>
  </div>
</div>

<div class="site-section">
  <div class="container">
    <div class="row mb-5">
      <div class="col-md-12">
        <div class="border p-4 rounded" role="alert">
          @if(session()->has('message'))
          <div class="alert alert-success">
            <strong>{{session()->get('message')}}</strong>
          </div>
          @endif
          Returning customer? <a href="#">Click here</a> to login
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 mb-5 mb-md-0">
        
        <div class="row mb-5">
          <div class="col-md-12">
            <h2 class="h3 mb-3 text-black">Your Order</h2>
            <div class="p-3 p-lg-5 border">
              <table class="table site-block-order-table mb-5">
                <thead>
                  <th>Product</th>
                  <th>Total</th>
                </thead>
                <tbody>
                  @foreach(Cart::content() as $item)
                  <tr>
                    <td>{{$item->name}} <strong class="mx-2">x</strong> {{$item->qty}}</td>
                    <td>{{$item->price}} each</td>
                  </tr>
                  @endforeach
                  <td class="text-black font-weight-bold"><strong>Cart Subtotal</strong></td>
                  <td class="text-black">{{Cart::subtotal()}}</td>
                  </tr>
                  <tr>
                    <td class="text-black font-weight-bold"><strong>Order Total</strong></td>
                    <td class="text-black font-weight-bold"><strong>{{Cart::total()}}</strong></td>
                  </tr>
                </tbody>
              </table>

              <div class="border p-3 mb-3">
                <h3 class="h6 mb-0"><a class="d-block" data-toggle="collapse" href="#collapsebank" role="button" aria-expanded="false" aria-controls="collapsebank">Direct Bank Transfer</a></h3>

                <div class="collapse" id="collapsebank">
                  <div class="py-2">
                    <p class="mb-0">Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>
                  </div>
                </div>
              </div>

              <div class="border p-3 mb-3">
                <h3 class="h6 mb-0"><a class="d-block" data-toggle="collapse" href="#collapsecheque" role="button" aria-expanded="false" aria-controls="collapsecheque">Cheque Payment</a></h3>

                <div class="collapse" id="collapsecheque">
                  <div class="py-2">
                    <p class="mb-0">Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>
                  </div>
                </div>
              </div>

              <div class="border p-3 mb-5">
                <h3 class="h6 mb-0"><a class="d-block" data-toggle="collapse" href="#collapsepaypal" role="button" aria-expanded="false" aria-controls="collapsepaypal">Paypal</a></h3>

                <div class="collapse" id="collapsepaypal">
                  <div class="py-2">
                    <p class="mb-0">Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>
                  </div>
                </div>
              </div>
              <form method="POST" action="{{route('confirm-order')}}" id="submitter">
                {{ csrf_field() }}
                <input type="hidden" name="cost" id="cost" value="{{Cart::total()}}">
                <input type="hidden" name="cart" value="{{Cart::content()}}">

                <input type="hidden" name="user_id" value="{{$user->id}}">
                <input type="hidden" name="email"  id="email" value="{{$user->email}}">
                <input type="hidden" name="phone" id="phone" value="{{$user->phone}}">
                
                <input type="hidden" name="transaction_ref" id="transaction_ref" value="$_COOKIE['transaction_ref']">


                <div class="form-group" style="color:white">
                  <a class="btn btn-primary btn-lg py-3 btn-block col-md-6" onclick="payWithPaystack()" data-toggle="modal" id="">Place Order</a>
                  <!-- <button class="btn btn-primary btn-lg py-3 btn-block" onclick="window.location='thankyou.html'">Place Order</button> -->
                </div>
              </form>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!-- </form> -->
  </div>
</div>

<script src="https://js.paystack.co/v1/inline.js"></script>

<script>
  function hold() {
    document.getElementById("booktype").value = "onhold";
    document.getElementById("submitter").submit();
  }

  function payWithPaystack() {

    var raw_string_number = $('#cost').val();
    var clean_string_number = raw_string_number.replace(',', '');
    var clean_number = parseInt(clean_string_number);
    var url = "https://paystack.ng/charge/verification";

    console.log(clean_number);
    var handler = PaystackPop.setup({
      key: 'pk_test_27de415452e0decc11cca9344a9e29c02b9c09d7',
      email: $('#email').val(),
      amount: (clean_number * 100),
      ref: '' + Math.floor((Math.random() * 1000000000) + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
      metadata: {
        custom_fields: [{
          phone: $('#phone').val(),

        }]
      },
      callback: function(response) {
        console.log(response);
        $('#transaction_ref').val(response.reference);
        $('#status').val(response.status);

        document.cookie = "status = " + status;
        $("#paystack_message").show();
        $("#hold_message").hide();
        document.getElementById("submitter").submit();

        //window.location = "http://localhost:8000/payment_success/" + response.reference;


      }
    });
    handler.openIframe();
  }
</script>
@stop