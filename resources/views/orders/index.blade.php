@extends('layouts.app')
@include('layouts.includes.navbar')

<div class="container">
	<div class="check">	 
		<div class="col-md-9 cart-items">
@if(session()->has('message'))
<div class="alert alert-success">
  <strong>{{session()->get('message')}}</strong> 
</div>
@endif
				
             <div class="single_top">
	 <div class="container"> 
	    <div class="register">
			  <div class="col-md-6 login-right">
			  	<h3>Shipping Information</h3>
				<div>
					
					<form name="register" method="POST" action="{{route('update-details')}}"> 
				    {{ csrf_field() }}
					<div>
					<span>Shipping Address<label>*</label></span>
					<textarea  class="form-control" name="address"  required>{{$address}}</textarea>				  </div>
						<span> Phone<label>*</label></span>
						<input type="text"class="form-control" name="phone" value="{{$phone}}" required > 
					 </div>
								<input type="submit" class="btn btn-danger btn-large" value="Update Shiiping Information">					
							
			</form>
			   </div>	
			   
		</div>
     </div>
</div>      
		 </div>
		 <div class="col-md-3 cart-total">
			 <a class="continue" href="{{route('shop')}}">Continue to basket</a>
			 <div class="price-details">
				 <h3>Price Details</h3>
				 <span>Total</span>
				 <span class="total1">{{Cart::subtotal()}}</span>
				 <div class="clearfix"></div>				 
			 </div>	
			 <ul class="total_price">
			   <li class="last_price"> <h4>TOTAL</h4></li>	
			   <li class="last_price"><span>{{Cart::total()}}</span></li>
			   <div class="clearfix"> </div>
			 </ul>
			
			 <div class="clearfix"></div>
			 <form method="POST" action="{{route('confirm-order')}}" id="submitter">
			 {{ csrf_field() }}
			 <input type="hidden" name="cost" id="cost" value="{{Cart::total()}}">
			 <input type="hidden" name="email" id="email" value="{{$email}}">
			 <input type="hidden" name="phone" id="phone" value="{{$phone}}">
			 <input type="hidden" name="address" value="{{$address}}">
			 <input type="hidden" name="cart" value="{{Cart::content()}}">
			 <input type="hidden" name="transaction_ref" id="transaction_ref" value="$_COOKIE['transaction_ref']">
			 <a class="order" onclick="payWithPaystack()"data-toggle="modal" id="" >Place Order</a>
			 <div class="total-item">
				 <h3>OPTIONS</h3>
				 <h4>COUPONS</h4>
				 <a class="cpns" href="#">Apply Coupons</a>
				 <p><a href="#">Log In</a> to use accounts - linked coupons</p>
			 </div>
			
			 </form>
			</div>
	 </div>
</div>
<script src="https://js.paystack.co/v1/inline.js"></script>

<script>

    function hold() {
        document.getElementById("booktype").value = "onhold";
        document.getElementById("submitter").submit();
    }

    function payWithPaystack(){

      var raw_string_number =  $('#cost').val();
        var clean_string_number = raw_string_number.replace(',','');
       var clean_number =  parseInt(clean_string_number);
        var url = "https://paystack.ng/charge/verification";

        console.log(clean_number);
        var handler = PaystackPop.setup({
            key: 'pk_test_27de415452e0decc11cca9344a9e29c02b9c09d7',
            email: $('#email').val(),
            amount: (clean_number * 100),
            ref: ''+Math.floor((Math.random() * 1000000000) + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
            metadata: {
                custom_fields: [
                    {
                        phone: $('#phone').val(),

                    }
                ]
            },
            callback: function(response){
                console.log(response);
                $('#transaction_ref').val(response.reference);
                $('#status').val(response.status);

                document.cookie = "status = " + status;
                $("#paystack_message").show();
                $("#hold_message").hide();
                document.getElementById("submitter").submit();

                //window.location = "http://localhost:8000/payment_success/" + response.reference;


            }
        });
        handler.openIframe();
    }
</script>

