<div class="header_top">
    <div class="container">
        <div class="one-fifth column row_1">
			<span class="selection-box"><select class="domains valid" name="domains">
		       <option>English</option>
		       <option>French</option>
		       <option>German</option>
		    </select></span>
        </div>
        <div class="cssmenu">
            <ul>
                <li class="active"><a href="{{ route('login')}}">My Account</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="header_bottom">
    <div class="container">
        <div class="col-xs-8 header-bottom-left">
            <div class="col-xs-2 logo">
                <h1><a href="{{'/'}}"><span>Tolad</span></a></h1>
            </div>
            <div class="col-xs-6 menu">
                <ul class="megamenu skyblue">
                    <li class="active grid"><a class="color1" href="">Men</a><div class="megapanel">
                            <div class="row">
                                <div class="col1">
                                    <div class="h_nav">
                                        <ul>
                                            <li><a href="">Accessories</a></li>
                                            <li><a href="">Bags</a></li>
                                            <li><a href="">Caps & Hats</a></li>
                                            <li><a href="">Hoodies & Sweatshirts</a></li>
                                            <li><a href="">Jackets & Coats</a></li>
                                            <li><a href="">Jeans</a></li>
                                            <li><a href="">Jewellery</a></li>
                                            <li><a href="">Jumpers & Cardigans</a></li>
                                            <li><a href="">Leather Jackets</a></li>
                                            <li><a href="">Long Sleeve T-Shirts</a></li>
                                            <li><a href="">Loungewear</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col1">
                                    <div class="h_nav">
                                        <ul>
                                            <li><a href="">Shirts</a></li>
                                            <li><a href="">Shoes, Boots & Trainers</a></li>
                                            <li><a href="">Shorts</a></li>
                                            <li><a href="">Suits & Blazers</a></li>
                                            <li><a href="">Sunglasses</a></li>
                                            <li><a href="">Sweatpants</a></li>
                                            <li><a href="">Swimwear</a></li>
                                            <li><a href="">Trousers & Chinos</a></li>
                                            <li><a href="">T-Shirts</a></li>
                                            <li><a href="">Underwear & Socks</a></li>
                                            <li><a href="">Vests</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col1">
                                    <div class="h_nav">
                                        <h4>Popular Brands</h4>
                                        <ul>
                                            <li><a href="">Levis</a></li>
                                            <li><a href="">Persol</a></li>
                                            <li><a href="">Nike</a></li>
                                            <li><a href="">Edwin</a></li>
                                            <li><a href="">New Balance</a></li>
                                            <li><a href="">Jack & Jones</a></li>
                                            <li><a href="">Paul Smith</a></li>
                                            <li><a href="">Ray-Ban</a></li>
                                            <li><a href="">Wood Wood</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="grid"><a class="color2" href="#">Women</a>
                        <div class="megapanel">
                            <div class="row">
                                <div class="col1">
                                    <div class="h_nav">
                                        <ul>
                                            <li><a href="">Accessories</a></li>
                                            <li><a href="">Bags</a></li>
                                            <li><a href="">Caps & Hats</a></li>
                                            <li><a href="">Hoodies & Sweatshirts</a></li>
                                            <li><a href="">Jackets & Coats</a></li>
                                            <li><a href="">Jeans</a></li>
                                            <li><a href="">Jewellery</a></li>
                                            <li><a href="">Jumpers & Cardigans</a></li>
                                            <li><a href="">Leather Jackets</a></li>
                                            <li><a href="">Long Sleeve T-Shirts</a></li>
                                            <li><a href="">Loungewear</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col1">
                                    <div class="h_nav">
                                        <ul>
                                            <li><a href="">Shirts</a></li>
                                            <li><a href="">Shoes, Boots & Trainers</a></li>
                                            <li><a href="">Shorts</a></li>
                                            <li><a href="">Suits & Blazers</a></li>
                                            <li><a href="">Sunglasses</a></li>
                                            <li><a href="">Sweatpants</a></li>
                                            <li><a href="">Swimwear</a></li>
                                            <li><a href="">Trousers & Chinos</a></li>
                                            <li><a href="">T-Shirts</a></li>
                                            <li><a href="">Underwear & Socks</a></li>
                                            <li><a href="">Vests</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col1">
                                    <div class="h_nav">
                                        <h4>Popular Brands</h4>
                                        <ul>
                                            <li><a href="">Levis</a></li>
                                            <li><a href="">Persol</a></li>
                                            <li><a href="">Nike</a></li>
                                            <li><a href="">Edwin</a></li>
                                            <li><a href="">New Balance</a></li>
                                            <li><a href="">Jack & Jones</a></li>
                                            <li><a href="">Paul Smith</a></li>
                                            <li><a href="">Ray-Ban</a></li>
                                            <li><a href="">Wood Wood</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li><a class="color4" href="about.html">About</a></li>
                    <li><a class="color5" href="404.html">Blog</a></li>
                    <li><a class="color6" href="contact.html">Support</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-4 header-bottom-right">
            <div class="box_1-cart">
                <div class="box_11"><a href="{{route('cart')}}">
                        <h4><p>Cart:&#x20a6;{{Cart::total()}} <span class=""></span> (<span id="" class=""></span> ({{Cart::count()}}Item)</p><img src="images/bag.png" alt=""/><div class="clearfix"> </div></h4>
                    </a></div>
                <p class="empty"><a href="{{route('empty')}}" class="">Empty Cart</a></p>
                <div class="clearfix"> </div>
            </div>
            <div class="search">
                <input type="text" name="s" class="textbox" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
                <input type="submit" value="Subscribe" id="submit" name="submit">
                <div id="response"> </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>