@extends('layouts.app')
@include('layouts.includes.navbar')
<div class="single_top">
	 <div class="container"> 
	     <div class="register">
		  	  <form name="register" method="POST" action="{{route('newuser')}}"> 
				{{ csrf_field() }}
				 <div class="register-top-grid">
					<h3>PERSONAL INFORMATION</h3>
					 <div>
						<span> Name<label>*</label></span>
						<input type="text"class="form-control" name="name" value="" > 
					 </div>
					 <div>
						 <span>Email Address<label>*</label></span>
						 <input type="email"class="form-control" name="email" value="" required> 
					 </div>
					         <div>
								<span>Password<label>*</label></span>
								<input class="form-control" type="password" name="password" value="" required>
								<div>
								<input type="submit" class="btn btn-danger btn-large" value="Register">
								</div>
							 </div>
			
							
				      <div class="register-but">
				 
					
					   <div class="clearfix"> </div>
			</form>
				</div>
		   </div>
     </div>
</div>      