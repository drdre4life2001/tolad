

require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

const router = new VueRouter({ mode: 'history'});
new Vue(Vue.util.extend({ router })).$mount('#app');
import App from './App.vue';

new Vue(Vue.util.extend({ router }, App)).$mount('#app');
Vue.component('articles', require('./components/articles.vue'));

const app = new Vue({
    el: '#app'
});
