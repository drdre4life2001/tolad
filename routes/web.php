<?php
use Gloudemans\Shoppingcart\Facades\Cart;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/contact', function () {
   return view('pages/contact');
});

Route::get('/about', function () {
    return view('pages/about');
 });


 Route::match(['get', 'post'], '/', ['uses' => 'UserController@home', 'as' => 'home']);
Route::match(['get', 'post'], '/login', ['uses' => 'UserController@index', 'as' => 'login']);
Route::match(['get', 'post'], '/logout', ['uses' => 'UserController@index', 'as' => 'login']);

Route::match(['get', 'post'], '/shop', ['uses' => 'ShoppingController@index', 'as' => 'shop']);
Route::match(['get', 'post'], '/checkout', ['uses' => 'ShoppingController@checkout', 'as' => 'checkout']);
Route::match(['get', 'post'], '/order', ['uses' => 'OrderController@index', 'as' => 'order']);
Route::match(['get', 'post'], '/shipping', ['uses' => 'ShoppingController@shipping', 'as' => 'shipping']);

// Route::middleware('auth')->group(function () {
 
//});

Route::match(['get', 'post'], '/cart', ['uses' => 'CartController@index', 'as' => 'cart']);
Route::match(['get', 'post'], '/shop/{product}', ['uses' => 'ShoppingController@show', 'as' => 'shop.show']);
Route::post('/cart', 'CartController@store')->name('cart.store');
Route::get('empty','CartController@destroy')->name('empty' );
Route::match(['get', 'post'], '/order', ['uses' => 'OrderController@index', 'as' => 'order']);
Route::match(['get', 'post'], '/newuser', ['uses' => 'UserController@create', 'as' => 'newuser']);
Route::match(['get', 'post'], '/update-datails', ['uses' => 'UserController@update', 'as' => 'update-details']);
Route::match(['get', 'post'], '/confirm-order', ['uses' => 'OrderController@create', 'as' => 'confirm-order']);

//remove cart item
Route::match(['get', 'post'], '/cart/{id}', ['uses' => 'CartController@remove', 'as' => 'remove']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/register', 'HomeController@register')->name('register');

