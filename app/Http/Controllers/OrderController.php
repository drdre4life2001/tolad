<?php

namespace App\Http\Controllers;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Auth;
use App\Order;
use App\User;
use App\Helpers\Paystack;
use DB;
class OrderController extends Controller
{


    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $address = Auth::user()->address;
        $phone = Auth::user()->phone;
        $email = Auth::user()->email;
        return view('orders.index', compact('address','phone', 'email'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
           //dd($request->all());

          
           //create user if not a returning customer
            $verify = new Paystack;
            $status  = $verify->verify($request->transaction_ref); 
            if($status =='true'){
               $carts = $request->cart;
                 $carts =  json_decode($carts, true);
                foreach ($carts as $cart){
                    $tax = $cart['tax'];
                    $price = $cart['price'];
                    $total = $tax + $price;
                    $user_id  = $request->user_id;
                    $new =  Order::create([
                        'customer_id'=>$user_id,
                        'name' => $cart['name'],
                        'quantity' => $cart['qty'],
                        'price' => $price,
                        'total' => $total,
                        'product_id' => $cart['id'],
                        ]);
                        
                }
                if(isset($new)){
                    Cart::destroy();
                    return view('orders.confirmation');

                }   

            }else{
                return redirect()->back()->with('message', 'Payment failed and order could not be confirmed');
            }

        }

        // return view('orders.confirmation');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
