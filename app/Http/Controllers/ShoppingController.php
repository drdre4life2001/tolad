<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Auth;
use App\User;
use DB;
class ShoppingController extends Controller
{  
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::inRandomOrder()->get()->take(5);
        return view('shopping.shop')->with('products', $products);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cart()
    {
        
     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($name)
    {
        //dd($request);
       //$name = 'sugar';
        $product = Product::where('name', $name)->firstOrFail();
        $slug = $product->slug;
        $maylike = Product::inRandomOrder()->get()->take(4);
        return view('/shopping.single', compact('product', 'maylike','slug'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function checkout()
    {
        


           // if(){}
            // $address = Auth::user()->address;
            // $phone = Auth::user()->phone;
            // $email = Auth::user()->email;
            return view('shopping.checkout', compact('address','phone', 'email'));
    
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function shipping(Request $request)
    {
        //
        if($request->isMethod('post'))
        {
           
        
            $user = DB::table('users')->where('email', $request->c_email)->first();
            if(empty($user)){ 
                $name =  $request->c_fname ." ".  $request->c_lname;  
             $user =  User::create([
                 
                 'name' => $name,
                 'email' => $request->c_email,
                 'address' => $request->address,
                 'phone' => $request->phone
                 ]);
                 
            }
           
            if($user){
               $user_id = $user->id;
                return  view('shopping.checkout', compact('user'));
            }

        }
        return view('shopping.shipping');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
